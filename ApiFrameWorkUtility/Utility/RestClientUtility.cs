﻿
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace ApiFrameworkUtili.Utility
{
    public class RestClientUtility
    {
        private static RestClient _RestClient;
        private static RestRequest _RestRequest;
        public static RestClient AccountRestClient
        {
            get
            {
                if (_RestClient == null)
                {
                    return new RestClient(ConfigReader.GetValueFromConfig("AccountUri"));
                }
                else
                {
                    return _RestClient;
                }
            }
        }
        public static RestClient BooksRestClient
        {
            get
            {
                if (_RestClient == null)
                {
                    return new RestClient(ConfigReader.GetValueFromConfig("BooksUri"));
                }
                else
                {
                    return _RestClient;
                }
            }
        }
        public static RestRequest CreateRequest(string resource, Method method)
        {
            if (_RestRequest == null)
            {
                return new RestRequest(resource, method);
            }
            else
            {
                return _RestRequest;
            }
        }
        public static T Post<T>(string resource, string payLoad)
        {
            return JsonConvert.DeserializeObject<T>(
            AccountRestClient.Execute
                (
                  CreateRequest(resource, Method.Post)
                  .AddBody(payLoad)
                ).Content);
        }
        public static T Get<T>(string resource)
        {
            return JsonConvert.DeserializeObject<T>(
            BooksRestClient.Execute
                (
                  CreateRequest(resource, Method.Get)
                ).Content);
        }

    }
}
