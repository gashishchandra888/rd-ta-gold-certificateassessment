﻿using ApiFrameworkUtili.Utility;
using CreateUserLoginByApi.Request;
using CreateUserLoginByApi.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateUserLoginByApi.Utili
{
    public class PostUser
    {
        private static string details;
        public UserDetails PostUserData(string userName,string password)
        {
            details = User(userName,password);
           var response= RestClientUtility.Post<UserDetails>("User", details);
            return response;
        }
        
        public string User(string userName, string password)
        {
            UserloginDetails details=new UserloginDetails();
            details.UserName = userName;
            details.Password = password;
            return JsonConvert.SerializeObject(details);
             
        }
    }
}
