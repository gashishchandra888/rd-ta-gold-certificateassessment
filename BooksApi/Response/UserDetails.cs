﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateUserLoginByApi.Response
{
    public class UserDetails
    {
        public string userID { get; set; }
        public string userName { get; set; }
        public List<object> books { get; set; }
    }
}
