﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace UiValidation.Pages
{
    public class DashBoard
    {
        private readonly IWebDriver driver;
        public DashBoard(IWebDriver driver)
        {
            this.driver = driver;
        }
        private readonly By userName = By.Id("userName-value");
        public string UserName()
        {
            WaitUtility.Wait(driver, userName);
            return Text(driver.FindElement(userName));
        }
        
        public string Text(IWebElement element)
        {
            return element.Text;
        }
    }
}
