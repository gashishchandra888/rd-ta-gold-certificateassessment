﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UiValidation.Pages
{
    public class BookPage
    {
        private readonly IWebDriver driver;
        public BookPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        private readonly By searchBox = By.Id("searchBox");
        private readonly By searchButton = By.XPath("//span[@id='basic-addon2']//span");
        private readonly By title = By.XPath("//span//child::a ");
        private readonly By author = By.XPath("(//div[@class='rt-td']//following-sibling::div//following-sibling::div)[1]");
        private readonly By publisher = By.XPath("(//div[@class='rt-td']//following-sibling::div//following-sibling::div)[2]");

        public void SearchForBook(string title)
        {
            driver.FindElement(searchBox).SendKeys(title);
        }
        public void Search()
        {
            Click(driver.FindElement(searchButton));
        }
        public void Click(IWebElement element)
        {
            element.Click();
        }
        public string Title()
        {
            return Text(driver.FindElement(title));
        }
        public string Author()
        {
            return Text(driver.FindElement(author));
        }
        public string Publisher()
        {
            return Text(driver.FindElement(publisher));
        }
        public string Text(IWebElement element)
        {
            return element.Text;
        }
    }
}
