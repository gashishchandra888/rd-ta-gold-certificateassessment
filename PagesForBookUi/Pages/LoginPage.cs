﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace UiValidation.Pages
{
    public class LoginPage
    {
        private readonly IWebDriver driver;
       public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        private readonly By username = By.Id("userName");
        private readonly By password=By.Id("password");
        private readonly By loginButton = By.Id("login");
        public void EnterUserName(string userName)
        {
            WaitUtility.Wait(driver, username);
            driver.FindElement(username).SendKeys(userName);
        }
        public void EnterPassword(string passcode)
        { 
            WaitUtility.Wait(driver,password);
            driver.FindElement(password).SendKeys(passcode);
        } 
        
        public void Login()
        {
            Scroll(driver.FindElement(loginButton));
            WaitUtility.Wait(driver, loginButton);
            Click(driver.FindElement(loginButton));
        }
       public void Scroll(IWebElement element)
        {
            ((IJavaScriptExecutor)driver)
            .ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }
        public void Click(IWebElement element)
        {
            element.Click();
        }
        public void BrowserActions(string url)
        {
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();
        }
       
    }
}
