﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class ConfigReader
    {
        public static IConfiguration? Configuration { get; set; }
        public static IConfiguration Read()
        {
            Configuration=new ConfigurationBuilder()
                .AddJsonFile(@"C:\\Users\\Ashishchandra_Gunnam\\source\\repos\\MiniProject\\AppSettings.json")
                .Build();
            return Configuration;
        }
        public static string GetValueFromConfig(string key)
        {
            if(string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException(nameof(key));

            }
            if(Configuration==null)
            {
                Configuration=Read();
            }
            return Configuration[key];
        }
    }
}
