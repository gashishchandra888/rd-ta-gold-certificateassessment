﻿using log4net;
using TechTalk.SpecFlow;
using Utility;

namespace MiniProject.Hooks
{
    [Binding]
    public sealed class Reports :ReportUtility
    {
       // static ILog log;
      
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            string path = ConfigReader.GetValueFromConfig("Path");
            BeforeTestRuns(@"path", "TestReport");
        }
        [BeforeFeature]
        public static void BeforeFeature(FeatureContext context)
        {
            BeforeFeatureStart(context);
        }
        [BeforeScenario]
        public void BeforeScenario(ScenarioContext context)
        {
            BeforeScenarioStart(context);
        }
        [AfterStep]
        public void AfterStep(ScenarioContext context)
        {
            AfterEachStep();
        }
        [AfterScenario]
        public static void AfterFeature(FeatureContext context)
        {
             //log.Error(context.TestError.Message);
        }
        [AfterTestRun]
        public static void AfterTestRun()
        {
            AfterTestRunCompletion();
        }
    }
}