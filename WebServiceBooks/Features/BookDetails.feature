﻿Feature: BookDetails

When i get the books i get list of books
so i should validate from api to ui

@tag1
Scenario: Book Deatails Validation
	Given I get the books from api 
	When I search for the same <book> in Ui
	| Book   |
	| <Book> |
	Then I should able to get the same book 
	And  I should verify Title
	And  I should verify Author
	And  I should verify Publisher
	Examples: 
	| Book                                      |
	| Git Pocket Guide                          |
	| Learning JavaScript Design Patterns       |
	| Designing Evolvable Web APIs with ASP.NET |
	| Speaking JavaScript                       |
	| You Don't Know JS                         |
	| Programming JavaScript Applications       |
	| Eloquent JavaScript, Second Edition       |
	| Understanding ECMAScript 6                |
