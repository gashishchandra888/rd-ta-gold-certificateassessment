﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceBooks.Drivers
{
    public class BrowserDriver : IDisposable
    {
        private readonly Lazy<IWebDriver> driver;
        private bool _isDisposed;

        public BrowserDriver()
        {
            driver = new Lazy<IWebDriver>(CreateWebDriver);
        }
        public IWebDriver Current => driver.Value;
        private IWebDriver CreateWebDriver()
        {
            return new ChromeDriver();
        }
        public void Dispose()
        {
            if (_isDisposed)
            {
                return;
            }
            if (driver.IsValueCreated)
            {
                Current.Quit();
            }
            _isDisposed = true;
        }
    }
}
