using CreateUserLoginByApi.Utili;

using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using UiValidation.Pages;
using Utility;
using WebServiceBooks.Drivers;

namespace MiniProject.StepDefinitions
{
    [Binding]
    public class LoginIntoBookStore
    {
        private ScenarioContext scenarioContext;
        private LoginPage login;
        private DashBoard dashBoard;
        LoginIntoBookStore(BrowserDriver driver, ScenarioContext scenarioContext)
        {
            login = new LoginPage(driver.Current);
            dashBoard = new DashBoard(driver.Current);
            this.scenarioContext = scenarioContext;
        }
        [Given(@"I navigate to login page")]
        public void GivenINavigateToLoginPage()
        {
            login.BrowserActions("https://demoqa.com/login");   
        }

        [When(@"I enter username and password")]
        public void WhenIEnterUsernameAndPassword()
        {
            string username = ConfigReader.GetValueFromConfig("UserName");
            string password = ConfigReader.GetValueFromConfig("password");
            login.EnterUserName(username);
            login.EnterPassword(password);
        }

        [When(@"I click on login button")]
        public void WhenIClickOnLoginButton()
        {
            login.Login();
        }

        [Then(@"I should verify the username on the dashboard")]
        public void ThenIShouldVerifyTheUsernameOnTheDashboard()
        {
            string usernameInUi = dashBoard.UserName();
            string usernameInApi = ConfigReader.GetValueFromConfig("UserName");
           Assert.That(usernameInUi, Is.EqualTo(usernameInApi),"Username is MisMatched with each Other");
        }
    }
}
