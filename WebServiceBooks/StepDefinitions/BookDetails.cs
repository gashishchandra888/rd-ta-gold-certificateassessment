using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using WebServiceBooks.Drivers;
using CreateUserLoginByApi.Utili;
using UiValidation.Pages;
using CreateUserLoginByApi.Response;
using Utility;

namespace MiniProject.StepDefinitions
{
    [Binding]
    public class BookDetails
    {

        private readonly ScenarioContext scenarioContext;
        private readonly FeatureContext featureContext;
        private readonly GetAllTheBooks books;
        private readonly BookPage book;
        private readonly LoginPage loginPage;
        private static int countbooks = 0;
        private static int count = -1;
        public BookDetails(ScenarioContext scenarioContext, GetAllTheBooks books,BrowserDriver driver, FeatureContext featureContext)
        {
            this.scenarioContext = scenarioContext;
            this.books = books;
            this.book = new BookPage(driver.Current);
            this.loginPage = new LoginPage(driver.Current);
            this.featureContext = featureContext;   
        }
        [Given(@"I get the books from api")]
        public void GivenIGetTheBooksFromApi()
        {
            scenarioContext.Add("Books", books.ListOfBooks());

        }
        [When(@"I search for the same <book> in Ui")]
        public void WhenISearchForTheSameBookInUi(Table table)
        {
            var row = table.Rows[0]["Book"];
            string url = ConfigReader.GetValueFromConfig("BooksUrl");
            loginPage.BrowserActions(url);
            featureContext.Add("key"+countbooks, ++count);
            book.SearchForBook(row);
        }

      

        [Then(@"I should able to get the same book")]
        public void ThenIShouldAbleToGetTheSameBook()
        {
            string titleInUi = book.Title();
            var books = scenarioContext.Get<Root>("Books");
            int listCount = featureContext.Get<int>("key"+countbooks);
            string titleInApi = books.books[listCount].title;
            Assert.That(titleInUi, Is.EqualTo(titleInApi),"Title MisMatched");
        }

        [Then(@"I should verify Title")]
        public void ThenIShouldVerifyTitle()
        {
            string titleInUI = book.Title();
            var books = scenarioContext.Get<Root>("Books");
            int listCount = featureContext.Get<int>("key" + countbooks);
            string titleInApi = books.books[listCount].title;
            Assert.That(titleInUI, Is.EqualTo(titleInApi), "Title MisMatched");
        }

        [Then(@"I should verify Author")]
        public void ThenIShouldVerifyAuthor()
        {
            string authorInUI = book.Author();
            var books = scenarioContext.Get<Root>("Books");
            int listCount = featureContext.Get<int>("key" + countbooks);
            string authorInApi = books.books[listCount].author;
            Assert.That(authorInUI, Is.EqualTo(authorInApi), "Author MisMatched");

        }

        [Then(@"I should verify Publisher")]
        public void ThenIShouldVerifyPublisher()
        {
            string publisherInUi = book.Publisher();
            var books = scenarioContext.Get<Root>("Books"); 
            int listCount = featureContext.Get<int>("key" + countbooks);
            string publisherInApi = books.books[listCount].publisher;
            Assert.That(publisherInUi, Is.EqualTo(publisherInApi), "Publisher MisMatched");
            countbooks++;   
        }
    }
}
