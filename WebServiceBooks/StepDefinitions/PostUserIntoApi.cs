using CreateUserLoginByApi.Response;
using CreateUserLoginByApi.Utili;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using Utility;

namespace MiniProject.StepDefinitions
{
    [Binding]
    public class PostUserIntoApi
    {
        private PostUser postUser;
        private ScenarioContext scenarioContext;
        PostUserIntoApi(PostUser postUserIntoApi,ScenarioContext scenarioContext)
        {
            this.postUser = postUserIntoApi;
            this.scenarioContext = scenarioContext;
        }


        [Given(@"I have username and password")]
        public void GivenIHaveUsernameAndPassword()
        {
            string username = ConfigReader.GetValueFromConfig("UserName");
            string password = ConfigReader.GetValueFromConfig("Password");
            scenarioContext.Add("UserName", username);
            scenarioContext.Add("Password", password);
        }

        [When(@"I post them into  given Api")]
        public void WhenIPostThemIntoGivenApi()
        {
            string userName = scenarioContext.Get<string>("UserName");
            string password = scenarioContext.Get<string>("Password");
           var response= postUser.PostUserData(userName, password);
            scenarioContext.Add("Response", response);
        }

        [Then(@"I should verify the status code")]
        public void ThenIShouldVerifyTheStatusCode()
        {
            var response = scenarioContext.Get<UserDetails>("Response");
           // Assert.That(response.StatusCode, Is.EqualTo(201));
        }

        [Then(@"I should verify the  responseBody")]
        public void ThenIShouldVerifyTheResponseBody()
        {
            var response = scenarioContext.Get<UserDetails>("Response");
            string userName= scenarioContext.Get<string>("UserName");
            Assert.That(response.userName, Is.EqualTo(userName),"Already user is present");
        }

    }
}
